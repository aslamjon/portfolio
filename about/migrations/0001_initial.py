# Generated by Django 3.0.7 on 2020-07-25 13:22

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='About',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('img', models.ImageField(upload_to='')),
                ('title', models.CharField(max_length=100)),
                ('description', models.TextField()),
                ('titel2', models.CharField(max_length=100)),
                ('description2', models.TextField()),
                ('title3', models.CharField(max_length=100)),
                ('description3', models.TextField()),
                ('title4', models.CharField(max_length=100)),
                ('email', models.CharField(max_length=100)),
                ('img2', models.ImageField(upload_to='')),
                ('onimg2', models.CharField(max_length=100)),
                ('onimgbtn', models.CharField(max_length=100)),
                ('description4', models.TextField(blank=True)),
            ],
        ),
    ]
