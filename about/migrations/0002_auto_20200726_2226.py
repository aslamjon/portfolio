# Generated by Django 3.0.7 on 2020-07-26 17:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('about', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='about',
            name='title5',
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.AlterField(
            model_name='about',
            name='description',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='about',
            name='description2',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='about',
            name='description3',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='about',
            name='email',
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.AlterField(
            model_name='about',
            name='img',
            field=models.ImageField(blank=True, upload_to=''),
        ),
        migrations.AlterField(
            model_name='about',
            name='img2',
            field=models.ImageField(blank=True, upload_to=''),
        ),
        migrations.AlterField(
            model_name='about',
            name='onimg2',
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.AlterField(
            model_name='about',
            name='onimgbtn',
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.AlterField(
            model_name='about',
            name='titel2',
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.AlterField(
            model_name='about',
            name='title',
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.AlterField(
            model_name='about',
            name='title3',
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.AlterField(
            model_name='about',
            name='title4',
            field=models.CharField(blank=True, max_length=100),
        ),
    ]
